package jabberwebchat.client;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import net.zschech.gwt.comet.client.CometClient;
import net.zschech.gwt.comet.client.CometListener;
import net.zschech.gwt.comet.client.CometSerializer;
import net.zschech.gwt.comet.client.SerialTypes;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.DivElement;
import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.SpanElement;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.DoubleClickEvent;
import com.google.gwt.event.dom.client.DoubleClickHandler;
import com.google.gwt.event.dom.client.KeyPressEvent;
import com.google.gwt.event.dom.client.KeyPressHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.Cookies;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Element;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.EventListener;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.WindowCloseListener;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class JabberWebChat implements EntryPoint {

	/**
	 * Create a remote service proxy to talk to the server-side Greeting service.
	 */
	private GreetingServiceAsync chatService;
	
	//user name, user id
	private String username;
	private int id;
	
    
	@SerialTypes( { ChatMessage.class, StatusUpdate.class})
    public static abstract class ChatCometSerializer extends CometSerializer {
    }
	
	private CometClient cometClient;
	
	//UI
	private HTML htmlChat;
	private TextBox boxSend;
	private ScrollPanel PanelChat;
	private AbsolutePanel LoginPanel;
	private AbsolutePanel ChatPanel;
	private Label labelInfo;
	private ListBox listUsers;
	private int counter = 0;
	private Image loading;
	private Image imgTopBarBack;
	private Image imgUsersBar;
	private Image imgbackCon;
	private Label btnTopic;
	private Label btnInfo;
	private Label btnExit;
	private Image btnJoin;
	private TextBox boxNick;
	
	/**
	 * This is the entry point method.
	 */
	@Override
	public void onModuleLoad() {
		
		chatService = GWT.create(GreetingService.class);

		//-----------------------UI----------------------
		//main panel
		RootPanel rootPanel = RootPanel.get("MainPanels");
		rootPanel.setSize("360", "640");
		
		//login panel
		LoginPanel = new AbsolutePanel();
		LoginPanel.setSize("640px", "360px");
		
		//background images
		Image backImg = new Image("images/background.jpg");
		backImg.setSize("640px", "360px");
		LoginPanel.add(backImg, 0, 0);
		
		//loading bar		
		loading = new Image("images/loading.gif");
		LoginPanel.add(loading, 116, 165);
		loading.setVisible(false);

		//label nick
		Label labelNick = new Label("Ваш ник:");
		labelNick.setStyleName("labelNick");
		LoginPanel.add(labelNick, 97, 90);
		labelNick.setSize("138px", "32px");
		
		//box login
		boxNick = new TextBox();
		boxNick.setStyleName("TextBoxNick");
		LoginPanel.add(boxNick, 76, 127);
		boxNick.setSize("180px", "25px");
		boxNick.setFocus(true);
		
		rootPanel.add(LoginPanel);
		
		//label info
		labelInfo = new Label();
		labelInfo.setStyleName("labelInfo");
		LoginPanel.add(labelInfo, 104, 240);
		
		//join button
		btnJoin = new Image("images/btn_join.png");
		btnJoin.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				labelInfo.setText("");
				
				//nick >=4
				if (boxNick.getText().length() >=4){
					loading.setVisible(true);
					btnJoin.setVisible(false);

					//login
					login(boxNick.getValue());
				} else labelInfo.setText("Слишком короткий ник");
			}
		});
		btnJoin.setStyleName("btn_join");
		LoginPanel.add(btnJoin, 103,200);
		
		//chat panel
		ChatPanel = new AbsolutePanel();
		ChatPanel.setVisible(false);
		rootPanel.add(ChatPanel, 0, 0);
		ChatPanel.setSize("640px", "360px");
		
		//background images 
		imgbackCon = new Image("images/backFullCon.jpg");
		ChatPanel.add(imgbackCon,0,29);
		
		//top background images
		imgTopBarBack = new Image("images/topBarBack.png");
		ChatPanel.add(imgTopBarBack,0,0);
		
		PanelChat = new ScrollPanel();
		PanelChat.setStyleName("PanelBoarder");
		ChatPanel.add(PanelChat, 10, 36);
		PanelChat.setSize("500px", "275px");
		
		boxSend = new TextBox();
		boxSend.setStyleName("boxSend");
		boxSend.addKeyPressHandler(new KeyPressHandler() {
			public void onKeyPress(KeyPressEvent event) {
				if (event.getNativeEvent().getKeyCode()==13)
				{	
					//send msg
					if (!boxSend.equals("")){
					sendMessage(boxSend.getValue());
					boxSend.setText("");
					}
				}
			}});
		
		ChatPanel.add(boxSend, 23, 325);
		boxSend.setSize("490px", "18px");
		
		//main chat panel
		htmlChat = new HTML();
		htmlChat.setStyleName("htmlChat");
		PanelChat.add(htmlChat);
		
		//users list
		listUsers = new ListBox();
		listUsers.setStyleName("usersList");
		listUsers.addDoubleClickHandler(new DoubleClickHandler() {
			public void onDoubleClick(DoubleClickEvent event) {
				boxSend.setText(boxSend.getText()+listUsers.getItemText(listUsers.getSelectedIndex())+" ");
				boxSend.setFocus(true);
			}
		});
		ChatPanel.add(listUsers, 520, 57);
		listUsers.setSize("110px", "281px");
		listUsers.setVisibleItemCount(5);
		
		//img for user list
		imgUsersBar = new Image("images/usersBar.png");
		ChatPanel.add(imgUsersBar,520,36);
		
		//button show topic
		btnTopic = new Label(null);
		btnTopic.setStyleName("imgTopic");
		ChatPanel.add(btnTopic, 25, 7);
		btnTopic.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				chatService.getTopic(id, new AsyncCallback<String>() {
					@Override
					public void onFailure(Throwable caught) {}

					@Override
					public void onSuccess(String result) {
						final DialogBox boxTopic = new DialogBox();
						Button btnClose = new Button("OK");
						VerticalPanel dialogPanel = new VerticalPanel();
						
						dialogPanel.addStyleName("dialogVPanel");
						dialogPanel.add(new HTML(result));
						dialogPanel.add(btnClose);
						
						boxTopic.setAnimationEnabled(false);
						boxTopic.setText("Тема");
						
						boxTopic.add(dialogPanel);
						btnClose.setFocus(true);
						
						btnClose.addClickHandler(new ClickHandler() {
							
							@Override
							public void onClick(ClickEvent event) {
								boxTopic.hide();
								boxSend.setFocus(true);
							}
						});
						boxTopic.center();	
					}
					
				});
			}
		});

		//button info
		btnInfo = new Label(null);
		btnInfo.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				Window.open("http://quake.im1x.com/about.html", "_blank", ""); 
			}
		});
		
		btnInfo.setStyleName("imgInfo");
		ChatPanel.add(btnInfo, 155, 7);
		
		//Exit button
		btnExit = new Label(null);
		btnExit.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				logout();
				LoginPanel.setVisible(true); ChatPanel.setVisible(false);
			}
		});
		btnExit.setStyleName("imgExit");
		ChatPanel.add(btnExit, 490, 7);
	};
	
	//login in comet
	private void loggedOn(String username) {
        this.username = username;
        System.out.println("logged in as " + username);
        CometSerializer serializer = GWT.create(ChatCometSerializer.class);
        cometClient = new CometClient(GWT.getModuleBaseURL() + "comet", serializer, new CometListener() {
                public void onConnected(int heartbeat) {
                        System.out.println("connected " + heartbeat);
                }
                @Override
                public void onDisconnected() {
                	System.out.println("disconnected");
    				logout();
    				LoginPanel.setVisible(true);
    				ChatPanel.setVisible(false);
    				labelInfo.setText("disconnected");        	
                }
                
                public void onError(Throwable exception, boolean connected) {
                	System.out.println("error " + connected + " " + exception);
    				logout();
    				LoginPanel.setVisible(true);
    				ChatPanel.setVisible(false);
    				labelInfo.setText("error " + connected + " " + exception);
                }
                
                public void onHeartbeat() {
                	System.out.println("heartbeat");
                }
                
                public void onRefresh() {
                	System.out.println("refresh");
                }

                @Override
				public void onMessage(List<? extends Serializable> messages) {
                	for (Serializable message : messages) {
                		if (message instanceof ChatMessage) {
                        ChatMessage chatMessage = (ChatMessage) message;
                                       
                        outmsg(chatMessage.getUsername(), chatMessage.getMessage());
                        }
                        else if (message instanceof StatusUpdate) {
                        	StatusUpdate statusUpdate = (StatusUpdate) message;
                            	if (statusUpdate.getText().equals("cleaning")) bRequest();
                            	else                                    	
                            		outstatus(statusUpdate.getText(), statusUpdate.getFlag());
                            }
                             else {
                                	System.out.println("unrecognised message " + message);
                                }
                        }
				}
        });
        cometClient.start();
}
	
	
	
	//login
    private void login(final String username) {
        chatService.login(username, new AsyncCallback<Boolean>() {
                
                @Override
                public void onSuccess(Boolean result) {
                	if (result){
                	loggedOn(username);
                	connect();
                	} else
                		labelInfo.setText("Вы уже в чате!");
                		labelInfo.setVisible(true);
                		loading.setVisible(false);
                }
                
                @Override
                public void onFailure(Throwable caught) {
                        System.out.println(caught.toString());
                }
        });
        
}

    //send msg
    private void sendMessage(String message) {
        chatService.send(message, id,  new AsyncCallback<Void>() {
                @Override
                public void onSuccess(Void result) {}
                
                @Override
                public void onFailure(Throwable caught) {
                        System.out.println(caught.toString());
                }
        });
}
	
	//print msg
    private void outmsg(String nick, String text){
    	
    	//time
    	SpanElement timeSpan = Document.get().createSpanElement();
    	Date time = new Date(); 
    	DateTimeFormat fmt = DateTimeFormat.getFormat("HH:mm:ss");
    	timeSpan.setInnerText("["+fmt.format(time)+"] ");
    	
    	//nick
    	SpanElement nickSpan = Document.get().createSpanElement();
    	nickSpan.setInnerText(nick+": ");
    	nickSpan.setAttribute("id", String.valueOf(counter));
    	nickSpan.setAttribute("nick", nick);
    	nickSpan.setAttribute("style", "cursor:pointer");

    	//text
    	SpanElement textSpan = Document.get().createSpanElement();
    	textSpan.setInnerHTML(text);
    	
    	//div
    	DivElement div = Document.get().createDivElement();
    	div.appendChild(timeSpan);
    	div.appendChild(nickSpan);
    	div.appendChild(textSpan);
    	
    	if (text.contains(username)) div.setAttribute("style", "color:red");

    	htmlChat.getElement().appendChild(div);
    	
    	//PanelScroll
    	PanelChat.scrollToBottom();
    	
    	//add click listener
        final Element elem = DOM.getElementById(String.valueOf(counter));
        
        // listener
        DOM.sinkEvents(elem, Event.ONCLICK);
        DOM.setEventListener(elem, new EventListener() {
            public void onBrowserEvent(final Event pEvent) {
            	boxSend.setText(boxSend.getText()+elem.getAttribute("nick")+" ");
            	boxSend.setFocus(true);
            }
        });
        counter++;
        
        if (counter>=15){
        	DOM.getParent(DOM.getElementById(String.valueOf(counter-15))).removeFromParent();
        }
        if (counter == 1) updatehUserList();;
        
    }
    
    //print status
    private void outstatus(String text, String flag){

    	//time
    	SpanElement timeSpan = Document.get().createSpanElement();
    	Date time = new Date(); 
    	DateTimeFormat fmt = DateTimeFormat.getFormat("HH:mm:ss");
    	timeSpan.setInnerText("["+fmt.format(time)+"] ");
    
    	//text
    	SpanElement textSpan = Document.get().createSpanElement();
    	textSpan.setInnerHTML(text);
    	textSpan.setAttribute("id", String.valueOf(counter));
    	
    	//div
    	DivElement div = Document.get().createDivElement();
    	div.appendChild(timeSpan);
    	div.appendChild(textSpan);
    	div.setAttribute("style", "color:"+flag);
    	
    	//html
    	htmlChat.getElement().appendChild(div);
    	
    	//PanelScroll
    	PanelChat.scrollToBottom();
    	
    	if (flag.equals("YellowGreen") || flag.equals("yellow")) updatehUserList();
    	
    	counter++;
    	if (counter>=15){
        	DOM.getParent(DOM.getElementById(String.valueOf(counter-15))).removeFromParent();
        }
    	
    }
    
    //update users list
    private void updatehUserList(){
    	chatService.getUsers(id, new AsyncCallback<ArrayList<String>>() {

			@Override
			public void onFailure(Throwable caught) {}

			@Override
			public void onSuccess(ArrayList<String> result) {
				listUsers.clear();
				for (int i = 0; i < result.size(); i++){
					listUsers.addItem(result.get(i));
				}

			}
    		
		});
    }
    
    
    
    //closing listener
    @SuppressWarnings("deprecation")
	public void closingListener(){
/*        Window.addWindowClosingHandler(new Window.ClosingHandler() {

            @Override
            public void onWindowClosing(ClosingEvent event) {

                   event.setMessage("У Вас открыт чат, закрытие или обновление страницы приведет к выходу с чата.");
                   

            }
        });
        
        Window.addCloseHandler(new CloseHandler<Window>() {

            @Override
            public void onClose(CloseEvent<Window> event) {
            	logout();
            	System.out.println("close|refresh");

            	
            }
        });*/
    	
    	Window.addWindowCloseListener(new WindowCloseListener() {

            @Override
            public String onWindowClosing() {
            	System.out.println("onWindowClosing");
                return "У Вас открыт чат, закрытие или обновление страницы приведет к выходу с чата.";//show a message
            }

            @Override
            public void onWindowClosed() {
            	logout();
            	System.out.println("onWindowClosed");
               // If user wishes to navigate away from the page do some stuff

            }
        });
    }

    //logout
    private void logout(){
    	chatService.logout(username, id, new AsyncCallback<Void>() {

			@Override
			public void onFailure(Throwable caught) {}

			@Override
			public void onSuccess(Void result) {
				id = -1;
				cometClient.stop();
				username = "";
			}
		});
    }
    
    //request for kick timeout users
    private void bRequest(){
    	chatService.backRequest(id, new AsyncCallback<Void>() {

			@Override
			public void onFailure(Throwable caught) {}

			@Override
			public void onSuccess(Void result) {}
		});
    }
    
    //connect and join in jabber chat
    private void connect(){
		chatService.connect_and_join(boxNick.getText(), new AsyncCallback<Integer>(){

			@Override
			public void onFailure(Throwable caught) {
				loading.setVisible(false);
				btnJoin.setVisible(true);
			}

			@Override
			public void onSuccess(Integer result) {
				if (result!=-1) {
				id = result;
				LoginPanel.setVisible(false); 
				ChatPanel.setVisible(true);
				boxSend.setFocus(true);
				closingListener();
				loading.setVisible(false);
				btnJoin.setVisible(true);
				} else {
					logout();
					loading.setVisible(false);
					btnJoin.setVisible(true);
					Cookies.removeCookie("JSESSIONID");
					labelInfo.setText("Такой ник уже занят, выберите другой");
				}
			}
		});
    }
}

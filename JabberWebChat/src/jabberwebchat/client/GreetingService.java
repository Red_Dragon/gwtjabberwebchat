package jabberwebchat.client;




import java.util.ArrayList;

import org.jivesoftware.smack.XMPPException;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;


/**
 * The client side stub for the RPC service.
 */
@RemoteServiceRelativePath("chat")
public interface GreetingService extends RemoteService {
	
	public Integer connect_and_join(String nick);
	
	public String getUsername() throws ChatException;
	
	public boolean login(String username) throws ChatException;
	
	public void logout(String username, int id) throws ChatException;
	
	public void send(String message, int id);
	
	public void setStatus(String text, String flag) throws ChatException;
	
	public ArrayList<String> getUsers(int id);
	
	public String getTopic(int id);
	
	public void backRequest(int id);

}

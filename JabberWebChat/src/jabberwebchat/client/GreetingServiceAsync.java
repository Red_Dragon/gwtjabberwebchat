package jabberwebchat.client;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 * The async counterpart of <code>GreetingService</code>.
 */
public interface GreetingServiceAsync {
	
	void connect_and_join(String nick, AsyncCallback<Integer> callback);
	
    public void getUsername(AsyncCallback<String> callback);
    
    public void login(String username, AsyncCallback<Boolean> callback);
    
    public void logout (String username, int id, AsyncCallback<Void> callback);
    
    public void send(String message, int id, AsyncCallback<Void> callback);
    
    public void setStatus(String text, String flag, AsyncCallback<Void> callback);
    
    void getUsers(int id, AsyncCallback<ArrayList<String>> callback);
    
    void getTopic(int id, AsyncCallback<String> callback);
    
    void backRequest(int id, AsyncCallback<Void> callback);
	
	}

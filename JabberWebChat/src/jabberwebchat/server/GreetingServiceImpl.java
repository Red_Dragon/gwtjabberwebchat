package jabberwebchat.server;

import jabberwebchat.client.ChatException;
import jabberwebchat.client.ChatMessage;
import jabberwebchat.client.GreetingService;
import jabberwebchat.client.JabberWebChat;
import jabberwebchat.client.StatusUpdate;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import javax.servlet.http.HttpSession;

import net.zschech.gwt.comet.server.CometServlet;
import net.zschech.gwt.comet.server.CometSession;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.PacketListener;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smackx.muc.MultiUserChat;
import org.jivesoftware.smackx.muc.ParticipantStatusListener;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

/**
 * The server side implementation of the RPC service.
 */

public class GreetingServiceImpl extends RemoteServiceServlet implements GreetingService {
	//connection settings
	private static ConnectionConfiguration connConfig = new ConnectionConfiguration("im1x.com",5222,"im1x.com");
	private static String conference = "online@conference.radio-t.com";
	
	//ArraysList for users, connections, active users
	private static ArrayList<XMPPConnection> xmppConList = new ArrayList<XMPPConnection>();
	private static ArrayList<MultiUserChat> mucList = new ArrayList<MultiUserChat>();
	private static ArrayList<Integer> activUsersId = new ArrayList<Integer>();
	
	//flag of first run
	private static boolean firstRun = true;
	
	//flag for correct rename
	boolean renameFlag = false;
	
	//Map for users and cometSessions
	private ConcurrentMap<String, CometSession> users = new ConcurrentHashMap<String, CometSession>();
	
	//log4j
	private static final Logger log = Logger.getLogger(GreetingServiceImpl.class);
	
	//join bot in chat, init log4j
    static {
		try {
		//configure bot, login and join
		xmppConList.add(new XMPPConnection(connConfig));
		xmppConList.get(0).connect();
		xmppConList.get(0).loginAnonymously();
        Presence presence = new Presence(Presence.Type.available);
        xmppConList.get(0).sendPacket(presence);
        mucList.add(new MultiUserChat(xmppConList.get(0), conference));
       	mucList.get(0).join("WebChat");
       	
       	//log4j cfg
       	PropertyConfigurator.configure("log4j.properties");
		} catch (XMPPException e) {
			log.error("static: "+e);
		}
    }
    
	/**
	 * @see JabberWebChat.client.ChatService#login(java.lang.String)
	 */
	@Override
	public boolean login(String username) {
			try {
				
	        // Get or create the HTTP session for the browser
			HttpSession httpSession = getThreadLocalRequest().getSession();
		
		  	if (!httpSession.isNew()){
		  			if (!nickCheck(httpSession.getAttribute("username").toString())) return false;
		  	}
		
	        // Get or create the Comet session for the browser
	        CometSession cometSession = CometServlet.getCometSession(httpSession);
	       
	        // Remember the user name for the
	        httpSession.setAttribute("username", username);
	        
	        log.info("User " + username + " (" + getThreadLocalRequest().getRemoteAddr() +")" + " logged");
	        
	        // setup the mapping of user names to CometSessions
	        if (users.putIfAbsent(username, cometSession) != null) {
	                // some one else has already logged in with this user name 
	                httpSession.invalidate();
	                throw new ChatException("User: " + username + " already logged in");
	        }
	        
			} catch (Exception e) {
				log.error("login: "+e);
			}

	        return true;

	}

	//connect and join at jabber server
	@Override
	public Integer connect_and_join(String nick) {

		int id = 0;
		
		//first run - exec listeners and cleaner
		if (firstRun) {
	       	listener(0);
	       	usersCleaner();
		}
		
		//add connections
		xmppConList.add(new XMPPConnection(connConfig));
		id = xmppConList.size()-1;

        try {
        	xmppConList.get(id).connect();
        } catch (XMPPException ex) {
            log.error("Failed to connect to " +  xmppConList.get(id).getHost()+" | " + ex);
            System.exit(1);
            return 1;
        }
        
        //login
        try {
        	 xmppConList.get(id).loginAnonymously();
		} catch (Exception e) {
			log.error("User:"+ nick + " | " + e);
		}
        
        Presence presence = new Presence(Presence.Type.available);
        xmppConList.get(id).sendPacket(presence);
            
        //join
        mucList.add(new MultiUserChat(xmppConList.get(id), conference));
        try {
        	mucList.get(id).join(nick);
		} catch (XMPPException e) {
			// if user with this nick is already joined
			log.error(e);
			if (e.getMessage().contains("conflict(409)")){
			xmppConList.get(id).disconnect();
			xmppConList.remove(id);
			mucList.remove(id);
			return -1;
			}
		}
        return id;
	}
	
	
	@Override
	public void logout(String username, int id) {
			try {
	        // check if there is a HTTP session setup.
	        HttpSession httpSession = getThreadLocalRequest().getSession(false);
	        if (httpSession == null) {
	                throw new ChatException("User: " + username + " is not logged in: no http session");
	        }
	        
	        // check if there is a Comet session setup. In a larger application the HTTP session may have been
	        // setup via other means.
	        CometSession cometSession = CometServlet.getCometSession(httpSession, false);
	        if (cometSession == null) {
	                throw new ChatException("User: " + username + " is not logged in: no comet session");
	        }
	        
	        // check the user name parameter matches the HTTP sessions user name
	        if (!username.equals(httpSession.getAttribute("username"))) {
	                throw new ChatException("User: " + username + " is not logged in on this session");
	        }
	        
	        log.info(username+" logout");
	        // remove the mapping of user name to CometSession
	        users.remove(username, cometSession);
	        
	        //disconnected
	        if (id!=-1){
	        xmppConList.get(id).disconnect();
	        xmppConList.set(id, null);
	        
	        //подумать
	        //httpSession.invalidate();
	        }
	        
			} catch (Exception e) {
				log.error("logout: "+e);
			}
	
	}

	/**
	 * @throws XMPPException 
	 * @see JabberWebChat.client.ChatService#send(java.lang.String)
	 */
	@Override
	public void send(String message, int id) {
		try {
			mucList.get(id).sendMessage(message);
		} catch (XMPPException e) {
			log.error("send: "+e);
		}
		
	}

	@Override
	public void setStatus(String text, String flag) {
	        try {
				
			// create the chat message
	        StatusUpdate statusUpdate = new StatusUpdate();
	        statusUpdate.setText(text);
	        statusUpdate.setFlag(flag);
	        
	        for (Map.Entry<String, CometSession> entry : users.entrySet()) {
	                entry.getValue().enqueue(statusUpdate);
	        }
	        
	        } catch (Exception e) {
	        	log.error("setStatus: "+e);
			}
	}
	
	
	@Override
    public String getUsername(){
            // check if there is a HTTP session setup.
			HttpSession httpSession = getThreadLocalRequest().getSession(false);
            if (httpSession == null) {
                    return null;
            }

            // return the user name
            return (String) httpSession.getAttribute("username");
    }
    
	//return list of users
	@Override
    public ArrayList<String> getUsers(int id){
		ArrayList<String> users = new ArrayList<>();
		Iterator<?> it = mucList.get(id).getOccupants();
	
	    while (it.hasNext()) {
	         users.add(clearNick((String) it.next()));
	    }
		return users;
	}

	//return topic
	@Override
	public String getTopic(int id){
		return textFormating(mucList.get(id).getSubject()+"");
		
	}
	
	//request for cleaner
	@Override
	public void backRequest(int id){
		if (!activUsersId.contains(id))
			activUsersId.add(id);
	}

	//listeners events chat
	private void listener(int id){
			firstRun = false;

			//message listener
			mucList.get(id).addMessageListener(new PacketListener() {
				
				@Override
				public void processPacket(Packet packet) {
					Message msg = (Message) packet;

					// create the chat message
		            ChatMessage chatMessage = new ChatMessage();
		            
		            chatMessage.setUsername(clearNick(msg.getFrom()));
		            chatMessage.setMessage(msg.getBody());
		            
		            for (Map.Entry<String, CometSession> entry : users.entrySet()) {
		                    entry.getValue().enqueue(chatMessage);
		            }
				}
			});
			
			//Status Listener
			mucList.get(id).addParticipantStatusListener(new ParticipantStatusListener() {
				
				@Override
				public void voiceRevoked(String arg0) {}
				
				@Override
				public void voiceGranted(String arg0) {}
				
				@Override
				public void ownershipRevoked(String arg0) {}
				
				@Override
				public void ownershipGranted(String arg0) {}
				
				@Override
				public void nicknameChanged(String arg0, String arg1) {
					try {
						setStatus(clearNick(arg0)+" сменил ник на  "+arg1, "Aquamarine");
					} catch (Exception e) {
						log.error("listener.nicknameChanged: "+e);
					}
					renameFlag = true;
				}
				
				@Override
				public void moderatorRevoked(String arg0) {}
				
				@Override
				public void moderatorGranted(String arg0) {}
				
				@Override
				public void membershipRevoked(String arg0) {}
				
				@Override
				public void membershipGranted(String arg0) {}
				
				@Override
				public void left(String arg0) {
					try {
						setStatus(clearNick(arg0)+ " вышел", "yellow");
					} catch (Exception e) {
						log.error("listener.left "+e);
					}
				}
				
				@Override
				public void kicked(String arg0, String arg1, String arg2) {
					try {
						setStatus(clearNick(arg0)+ " был кикнут по причине "+arg2, "red");
					} catch (Exception e) {
						log.error("listener.kicked: "+e);
					}
				}
				
				@Override
				public void joined(String arg0) {
					if (!renameFlag)
					try {
						setStatus(clearNick(arg0)+ " вошел", "YellowGreen");
					} catch (Exception e) {
						log.error("listener.joined: "+e);
					}
					renameFlag = false;
				}
				
				@Override
				public void banned(String arg0, String arg1, String arg2) {
					try {
						setStatus(clearNick(arg0)+ " был забанен по причине "+arg2, "red");
					} catch (Exception e) {
						log.error("listener.banned: "+e);
					}
				}
				
				@Override
				public void adminRevoked(String arg0) {}
				
				@Override
				public void adminGranted(String arg0) {}
			});
	
		}

	//sender msg for check on timeout
	private void usersCleanerSender() {
	 try {
		
     // create the chat message
     StatusUpdate statusUpdate = new StatusUpdate();
     statusUpdate.setText("cleaning");
     statusUpdate.setFlag("");
     
     for (Map.Entry<String, CometSession> entry : users.entrySet()) {
             entry.getValue().enqueue(statusUpdate);
     }
     
	 } catch (Exception e) {
		 log.error("usersCleanerSender: "+e);
		}
    }
    
    //timeout kicker
	private void usersCleaner(){
    	new Thread(new Runnable() {
			@Override
			public void run() {
				while (true){
					
					try {
						// need two sender, for correct work, cause packet lost
						Thread.sleep(30000);
						usersCleanerSender();
						Thread.sleep(1500);
						usersCleanerSender();
						Thread.sleep(1500);
						
						//check result
						for (int i = 1; i <= xmppConList.size() - 1; i++) {
							if (!activUsersId.contains(i) && xmppConList.get(i)!=null) {
								log.info(mucList.get(i).getNickname() + " TIMEOUT!");
					            users.remove(mucList.get(i).getNickname());
					            xmppConList.get(i).disconnect();
					            xmppConList.set(i, null);
							}
						}
						activUsersId.clear();
					} catch (Exception e) {
						log.error("usersCleaner: "+e);
					}
				}
			}
		}).start();
    }
    
	
	//check for already used nick
    private boolean nickCheck(String nick) {
    	if (!firstRun){
    	Iterator<?> it = mucList.get(0).getOccupants();
	    	while (it.hasNext()) {
	           if (clearNick((String) it.next()).equals(nick)){
	        	   return false;
	           }
	    	}
        } 
        	
    return true;	    	
    }
    
    //JID to nick
    private String clearNick(String from){
    	return from.substring(from.indexOf("/")+1, from.length());
    }
    
    //formating text for html
    private String textFormating(String text){
    	String stringFormating;
    	stringFormating = text.replaceAll("\n", "<br>");
    	
    	
/*    	//links
    	String firstPart = stringFormating.substring(0, text.indexOf("http://"));
    	stringFormating = stringFormating.replace(firstPart, "");
    	
    	String twoPart = stringFormating.substring(0, stringFormating.indexOf(" "));
    	stringFormating = stringFormating.replace(twoPart, "");
    	
    	
    	//String res = firstPart+"<a href=\""+twoPart+"\">"+twoPart+"</a>"+stringFormating;
    	
    	//stringFormating = new StringBuffer().insert(offset, b)					<a href="http://">Link text</a>		 \"Кавычки \"
    	System.out.println(firstPart);
    	System.out.println(twoPart);
    	System.out.println(stringFormating);
		return firstPart+"<a href="+twoPart+">"+twoPart+"</a>"+stringFormating;*/
    	return stringFormating;
    }
		
}

	
